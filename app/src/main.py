import logging
import argparse

from aiohttp import web

from settings import DEBUG

from routes import setup_routes
from database import redis_engine, test_redis_engine


logging.basicConfig(level=logging.DEBUG if DEBUG else logging.INFO)
logger = logging.getLogger('MAIN')


async def get_app(testing=False):
    logger.info(f'Starting {DEBUG} server')
    app = web.Application()
    if testing:
        app.cleanup_ctx.append(test_redis_engine)
    else:
        app.cleanup_ctx.append(redis_engine)

    if DEBUG and not testing:
        import aioreloader
        aioreloader.start()
    setup_routes(app)
    return app

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Offers parser")
    parser.add_argument(
        '-P', '--port',
        default=8080,
    )
    args = parser.parse_args()
    logger.info(f'DEVELOP: {DEBUG}')

    app = get_app()
    web.run_app(app, port=args.port)
