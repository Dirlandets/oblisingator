import logging
import aioredis
from settings import REDIS_HOST, REDIS_TEST_HOST

logger = logging.getLogger('DATABASE')


async def redis_engine(app):
    app['redis'] = await aioredis.create_redis_pool(REDIS_HOST)
    yield
    app['redis'].close()
    await app['redis'].wait_closed()


async def test_redis_engine(app):
    app['redis'] = await aioredis.create_redis_pool(REDIS_TEST_HOST)
    yield
    flush = await app['redis'].execute('FLUSHDB', encoding='utf-8')
    logger.info(f'Test Redis DB was flushed {flush}')
    app['redis'].close()
    await app['redis'].wait_closed()
