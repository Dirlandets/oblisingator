import unittest
from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

from main import get_app

class HealthCheck(AioHTTPTestCase):

    async def get_application(self):
        app = await get_app(testing=True)
        return app

    @unittest_run_loop
    async def test_its_alive(self):
        resp = await self.client.request("GET", "/")
        assert resp.status == 200
        text = await resp.text()
        assert "I'm working" in text


if __name__ == "__main__":
    unittest.main()
