import asyncio
from functools import wraps


def with_loop(f):
    @wraps(f)
    def inner(*args, **kwargs):
        coro = f(*args, **kwargs)
        if coro is not None:
            asyncio.run(coro)
    return inner
